﻿using UnityEngine;

public class Vida : MonoBehaviour
{
    public GameObject vidaj2;
    public GameObject vidaj1;
    public LuffyController jugador1;
    public YugiController jugador2;

    //Delegados que controlan la vida de los personajes
    void Start()
    {
        jugador1.DamageEvent += Daño;
        jugador2.DamageEvent += Daño2;
        cajaController.CurasEvent += Cura;
    }

    private void Cura(float vida)
    {
        if (jugador1.hp <= 90 && jugador1.hp > 0)
        {
            jugador1.hp += vida;
            vidaj1.transform.localScale = new Vector3(jugador1.hp / 100, vidaj1.transform.localScale.y);
        }
    }
    private void Daño(float vida)
    {
        vidaj1.transform.localScale = new Vector3(vida / 100, vidaj1.transform.localScale.y);
    }
    private void Daño2(float vida)
    {
        vidaj2.transform.localScale = new Vector3(vida / 100, vidaj2.transform.localScale.y);
    }
}
