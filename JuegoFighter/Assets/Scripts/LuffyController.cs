﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LuffyController : MonoBehaviour
{
    public bool stuneado = false;
    public int speed = 10;
    public bool salto = true;
    public int fuerzaSalto = 30000;
    public int lanzables = 10;
    public GameObject lanzable;
    public GameObject caja;
    public bool escudo = true;
    public float escudoHP = 50f;
    public bool escudoActivo;
    public int status;
    private bool pararCombo;
    private bool pegar;
    private bool cajaSpawn = true;
    public bool bloquear;

    public bool collSlifer;
    public bool collObelisk;
    public bool collRa;

    public float hp = 100;

    public Text YugiWin;

    //evento
    public delegate void Damage(float vida);
    public event Damage DamageEvent;

    // Start is called before the first frame update
    void Start()
    {
        escudo = true;
        escudoActivo = true;

        cajaSpawn = true;

        collSlifer = false;
        collObelisk = false;
        collRa = false;
    }

    void Update()
    {
        //Cuando el personaje fenece
        if (hp <= 0)
        {
            this.GetComponent<Animator>().SetBool("Bloquear", false);
            this.GetComponent<Animator>().SetBool("Mov", false);
            this.GetComponent<Animator>().SetTrigger("Muerte");
            Debug.Log("YugiWins");
            YugiWin.gameObject.SetActive(true);
            Destroy(this);
        }
        else
        {
            //Cuando el personaje cae del escenario
            if (this.transform.position.y < -50)
            {
                hp = 0;
                DamageEvent(hp);
            }
            //Pegar y combos
            if (Input.GetKeyDown("c"))
            {
                if (salto)
                {
                    if (!bloquear)
                    {
                        //Gatling
                        if (status == 1)
                        {
                            pegar = true;
                            GameObject hitboxPegar = this.gameObject.transform.GetChild(2).gameObject;
                            hitboxPegar.SetActive(true);
                            this.GetComponent<Animator>().SetBool("Mov", false);
                            this.GetComponent<Animator>().SetTrigger("Ataque2");
                            StartCoroutine(parar(1f, hitboxPegar));
                        }
                        else
                        {
                            pararCombo = true;
                            status = 0;
                        }
                        //Puñetazo normal
                        if (status == 0 && !pegar)
                        {
                            pegar = true;
                            GameObject hitboxPegar = this.gameObject.transform.GetChild(2).gameObject;
                            hitboxPegar.SetActive(true);
                            this.GetComponent<Animator>().SetBool("Mov", false);
                            this.GetComponent<Animator>().SetTrigger("Ataque1");
                            pararCombo = false;
                            StartCoroutine(MaxCombo(0.5f, status));
                            StartCoroutine(MinCombo(0.2f, 1));
                            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                            StartCoroutine(parar(0.8f, hitboxPegar));
                        }

                    }
                }
            }
            //Puñetazo largo
            if (Input.GetKeyDown("f"))
            {
                if (salto)
                {
                    this.GetComponent<Animator>().SetTrigger("Ataque3");
                }
            }
            //Spawneo de cajas
            if (Input.GetKeyDown("b") && cajaSpawn == true)
            {
                cajaSpawn = false;
                GameObject newBlock = Instantiate(caja);
                newBlock.transform.position = new Vector2(this.transform.position.x + 3, this.transform.position.y + 1);
                Debug.Log("antes" + cajaSpawn);
                StartCoroutine(cajaSpawneacion(0.5f));
                Debug.Log("despues " + cajaSpawn);
            }
            //Piedras
            if (Input.GetKeyDown("n"))
            {
                if (lanzables != 0)
                {
                    lanzables--;
                    GameObject newBlock = Instantiate(lanzable);
                    newBlock.transform.position = new Vector2(this.transform.position.x + 4, this.transform.position.y);
                    newBlock.GetComponent<Rigidbody2D>().velocity = new Vector2(5f, 0);
                    StartCoroutine(Piedras(3f));
                    newBlock.gameObject.name = "roca";
                }
            }

            //Movimiento del personaje
            if (stuneado == false && !pegar)
            {
                //Moverse a la derecha como españa
                if (Input.GetKey("d"))
                {
                    this.GetComponent<Animator>().SetBool("Mov", true);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(speed, this.GetComponent<Rigidbody2D>().velocity.y);
                    this.transform.localScale = new Vector2(1.5f, 1.5f);
                }
                //Moverse a la izquierda
                else if (Input.GetKey("a"))
                {
                    this.GetComponent<Animator>().SetBool("Mov", true);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
                    this.transform.localScale = new Vector2(-1.5f, 1.5f);
                }
                else
                {
                    this.GetComponent<Animator>().SetBool("Mov", false);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y);
                }
                //Saltar
                if (Input.GetKeyDown("w") && salto)
                {
                    this.GetComponent<Animator>().SetTrigger("Saltar");
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fuerzaSalto));
                    this.GetComponent<Animator>().SetBool("Mov", false);
                    salto = false;
                }
                //Bloquear
                else if (Input.GetKey("s"))
                {
                    if (escudo == true && escudoHP > 0)
                    {
                        bloquear = true;
                        escudoActivo = true;
                        StartCoroutine(Escudico(1));
                        this.GetComponent<Animator>().SetBool("Bloquear", true);
                        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y);
                    }
                }
                else
                {
                    bloquear = false;
                    escudoActivo = false;
                    this.GetComponent<Animator>().SetBool("Bloquear", false);
                }

            }
        }
    }

    //Combos
    IEnumerator parar(float f, GameObject ataque)
    {
        yield return new WaitForSeconds(f);
        if (pegar == true)
        {
            pegar = false;
        }
    }
    IEnumerator MinCombo(float f, int estado)
    {
        yield return new WaitForSeconds(f);
        if (!pararCombo)
        {
            status = estado;
        }

    }
    IEnumerator MaxCombo(float f, int estado)
    {
        yield return new WaitForSeconds(f);
        if (status != estado)
        {
            status = 0;
            pararCombo = true;
        }
    }
    IEnumerator parar2(float f)
    {
        yield return new WaitForSeconds(f);
        if (pegar == true)
        {
            pegar = false;
        }
    }

    //(esto no esta acabado)
    IEnumerator Escudico(float f)
    {
        yield return new WaitForSeconds(f);

        if (escudoHP == 0)
        {
            escudo = false;
            StartCoroutine(RegenerarEscudico(10));
        }

        else if (escudoHP > 0)
        {
        }

    }
    //(esto no esta acabado)
    IEnumerator RegenerarEscudico(float f)
    {
        yield return new WaitForSeconds(f);
        escudo = true;
        escudoHP = 50;
    }

    //cd Piedras
    IEnumerator Piedras(float f)
    {
        yield return new WaitForSeconds(f);
        if (lanzables >= 10)
        {
        }
        else
        {
            lanzables++;
        }
    }

    //CD Cajas
    IEnumerator cajaSpawneacion(float f)
    {
        yield return new WaitForSeconds(f);
        cajaSpawn = true;
    }

    //CD stun
    IEnumerator cdSlifer(float f)
    {
        yield return new WaitForSeconds(f);
        stuneado = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("muroabajo") || collision.gameObject.name == "caja")
        {
            salto = true;
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        //Colision con la hitbox de daño de Yugi
        if (collision.gameObject.name == "HitboxPegar" && !bloquear)
        {
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-350f, 100f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(350f, 100f));
            }
            hp -= 15;
            if (hp <= 0)
            {
                hp = 0;
            }
            DamageEvent(hp);
        }
        //Colision con la carta roja de Slifer el Productor Ejecutivo
        if (collision.gameObject.name == "Slifer" && !bloquear)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
            stuneado = true;
            hp -= 5;
            if (hp <= 0)
            {
                hp = 0;
            }
            DamageEvent(hp);
            StartCoroutine(cdSlifer(1));

        }
        //Colision con la carta Azul de Obelisk el Mamadisimo
        if (collision.gameObject.name == "Obelisk" && !bloquear)
        {
            if (this.transform.localScale.x == -1.5)
            {
                //player2 mira a la izquierda
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
                this.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 0));
                this.GetComponent<Rigidbody2D>().AddForce(new Vector3(90000, 60000));
                hp -= 5;
                if (hp <= 0)
                {
                    hp = 0;
                }
                DamageEvent(hp);
            }
            else if (this.transform.localScale.x == 1.5)
            {
                //player2 mira a la derecha
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
                this.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 0));
                this.GetComponent<Rigidbody2D>().AddForce(new Vector3(-150000, 60000));
                hp -= 5;
                if (hp <= 0)
                {
                    hp = 0;
                }
                DamageEvent(hp);
            }
        }
        //Colision con la carta Amarilla de Ra o tambien llamado Mega Ultra Chicken
        if (collision.gameObject.name == "Ra" && !bloquear)
        {
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-3050f, 100f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(3050f, 100f));
            }
            hp -= 10;
            if (hp <= 0)
            {
                hp = 0;
            }
            DamageEvent(hp);
        }


    }
}

