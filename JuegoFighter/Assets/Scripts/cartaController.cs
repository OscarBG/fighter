﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cartaController : MonoBehaviour
{
    //Si pega contra algo o se cae del mapa se destruye
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);

        if (collision.gameObject.CompareTag("muroabajo") || this.transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }
    }
}
