﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConchaVerde : MonoBehaviour
{
    #region Initializations
    public bool velocidad = true;
    public float velocidadYMax;
    #endregion Initializations

    #region Methods
    void Start()
    {
        velocidadYMax = 25;
    }

    void FixedUpdate()
    {
        if (this.GetComponent<Rigidbody>().velocity.y > velocidadYMax)
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(this.GetComponent<Rigidbody>().velocity.x, velocidadYMax, this.GetComponent<Rigidbody>().velocity.z);
        }
        if (velocidad == true)
        {
            this.GetComponent<Rigidbody>().AddRelativeForce(this.transform.localPosition * 200f * Time.deltaTime);
        }

        if (this.transform.position.y < -50f)
        {
            Destroy(this);
        }
        StartCoroutine(Wait(3f));
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cosos"))
        {
            velocidad = false;
        }
        if (collision.gameObject.CompareTag("Floor"))
        {
            this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionY; ;
        }

    }
    #endregion Methods

    #region Coroutines
    private IEnumerator Wait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(this);
    }
    #endregion Coroutines
}
