﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class PlayerController : MonoBehaviour
{
    #region Initializations
    public GameObject player;
    public GameObject player2;
    public GameObject ConchaVerde;
    public GameObject CajaMala;
    public GameObject Platano;
    public Material BaseMaterial;
    public Material StarMaterial;

    public bool jump;
    public bool invencible;
    public bool cajaOn;
    public bool objetoSpawn;
    public bool timer;
    public bool objetoObtenido;
    private bool hitted;
    public bool Morision;

    public int Globos;
    private int tiempoRespawnCajas;
    private int nObjeto;
    public int numObjetoDisponible;

    public List<GameObject> GlobosJugador;
    public GameObject CanvasJugador;
    #endregion Initializations

    #region Constants
    private float SPEED = 10000f;
    private float JUMP = 25000f;
    private float ROTATE_SPEED = 200f;
    #endregion Constants

    #region Fields
    private Vector3 leftBound;
    private Vector3 rightBound;
    #endregion Fields

    #region Methods
    private void Start()
    {
        invencible = false;
        Globos = 6;
        cajaOn = false;
        objetoSpawn = false;
        timer = false;
        numObjetoDisponible = 0;
        tiempoRespawnCajas = 15;
        Morision = false;
    }

    public void FixedUpdate()
    {
        // Si el jugador no ha muerto, puede moverse, saltar, etc
        if (Morision == false)
        {
            //Inputs del jugador
            ProcessInput();

            //Cuando cojes una caja tarda 3 segundos en darte el objeto, si ya has cogido una caja no puedes coger otra hasta que no uses el objeto
            if (cajaOn == true && timer == false)
            {
                timer = true;
                StartCoroutine(ObjetoRandomTimer(3f));
            }
            if (numObjetoDisponible < 0)
            {
                numObjetoDisponible = 0;
            }
        }
        //Si ha muerto no puede hacer nada
        else if (Morision == true && player2.GetComponent<PlayerController2>().Morision == false)
        {
            //no hace nada
        }

    }
    //TODO cuando cojes una caja puedes seguitr cogiendo cajas, cambiar

    private void ProcessInput()
    {
        if (Input.GetKey("a"))
        {
            /*
             * RotateAround, gira en funció d'un punt (fent servir un punt de referencia, un vector de rotació, i un angle)
             * El punt de referencia seria el centre de l'objecte. si vols rotar sobre tu mateix seria la propia position (recordem que la position implica les coordenades centrals)
             * el vector pots fer servir un dels tres vectors base
             *  transform.up -> gira al voltant de l'eix Y (fletxa verda)
             *  transform.forward -> gira al voltant de l'eix Z (fletxa blava)
             *  transform.right -> gira al voltant de l'eix X (fletxa vermella)
             * */

            /*
             * Time.deltaTime torna el temps entre Frames. D'aquesta forma t'assegures que la velocitat sigui independent del framerate. (un framerate mes baix fara que es mogui mes per frame)
             **/
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
        }
        if (Input.GetKey("d"))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
        }

        if (Input.GetKey("w")) { this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime); }
        if (Input.GetKey("s")) { this.GetComponent<Rigidbody>().AddForce(-this.transform.forward * SPEED * Time.deltaTime); }

        //Boton de saltar
        if (Input.GetKey("space"))
        {
            if (jump == true)
            {
                this.GetComponent<Rigidbody>().AddForce(this.transform.up * JUMP * Time.deltaTime);
                jump = false;
            }
        }
        //Si el jugador tiene un objeto puede usarlo 
        if (Input.GetKeyDown("v") && objetoSpawn == true)
        {
            //Mira el numero del objeto que ha salido en la caja
            if (nObjeto == 0)
            {
                //Spawnea una caja 
                SpawnCajaMala();
                objetoSpawn = false;
                objetoObtenido = false;
                CanvasJugador.GetComponent<Animator>().SetBool("CajaMala", false);
            }
            else if (nObjeto == 1)
            {
                //Spawnea una concha 
                SpawnConcha();
                objetoSpawn = false;
                objetoObtenido = false;
                CanvasJugador.GetComponent<Animator>().SetBool("Concha", false);
            }
            else if (nObjeto == 2)
            {
                //Spawnea tres conchas 
                SpawnConchax3();
                if (numObjetoDisponible == 0)
                {
                    objetoSpawn = false;
                    objetoObtenido = false;
                    CanvasJugador.GetComponent<Animator>().SetBool("Concha", false);
                }
            }
            else if (nObjeto == 3)
            {
                //Spawnea un platano 
                SpawnPlatano();
                objetoSpawn = false;
                objetoObtenido = false;
                CanvasJugador.GetComponent<Animator>().SetBool("Platano", false);
            }
            else if (nObjeto == 4)
            {
                //Spawnea tres platanos 
                SpawnPlatanox3();
                if (numObjetoDisponible == 0)
                {
                    objetoSpawn = false;
                    objetoObtenido = false;
                    CanvasJugador.GetComponent<Animator>().SetBool("Platano", false);
                }
            }
            else if (nObjeto == 5)
            {
                //Da velocidad al jugador en la direccion que esta mirando 
                SpawnSeta();
                objetoSpawn = false;
                objetoObtenido = false;
                CanvasJugador.GetComponent<Animator>().SetBool("Seta", false);
            }
            else if (nObjeto == 6)
            {
                //Da velocidad al jugador en la direccion que esta mirando (3 veces)
                SpawnSetax3();
                if (numObjetoDisponible == 0)
                {
                    objetoSpawn = false;
                    objetoObtenido = false;
                    CanvasJugador.GetComponent<Animator>().SetBool("Seta", false);
                }

            }
            else if (nObjeto == 7)
            {
                //Da velocidad al jugador en la direccion que esta mirando (10 veces)
                SpawnSetaDorada();
                if (numObjetoDisponible == 0)
                {
                    objetoSpawn = false;
                    objetoObtenido = false;
                    CanvasJugador.GetComponent<Animator>().SetBool("SetaD", false);
                }
            }
            else if (nObjeto == 8)
            {
                //Hace al jugador invulnerable a cualquier daño, si empuja al otro jugador le hace daño
                SpawnEstrella();
                objetoSpawn = false;
                objetoObtenido = false;
                CanvasJugador.GetComponent<Animator>().SetBool("Estrella", false);
            }
        }
    }

    //Funcion que mira que objeto ha salido de la caja, tambien cambia el sprite del objeto que se muestra en el canvas
    private void ObjetoRandom()
    {
        var r = UnityEngine.Random.Range(0, 5);
        //Caja mala
        if (r == 0)
        {
            nObjeto = 0;
            numObjetoDisponible = 1;
            CanvasJugador.GetComponent<Animator>().SetBool("CajaMala", true);
            Debug.Log("Objeto actual = CajaMala");
        }
        //Concha verde
        else if (r == 1)
        {
            var r2 = UnityEngine.Random.Range(0, 2);
            //Solo una concha
            if (r2 == 0)
            {
                nObjeto = 1;
                numObjetoDisponible = 1;
                CanvasJugador.GetComponent<Animator>().SetBool("Concha", true);
                Debug.Log("Objeto actual: 1 Concha");

            }
            //3 conchas
            else if (r2 == 1)
            {
                nObjeto = 2;
                numObjetoDisponible = 3;
                CanvasJugador.GetComponent<Animator>().SetBool("Concha", true);
                Debug.Log("Objeto actual: 3 Conchas");
            }

        }
        //Banana
        else if (r == 2)
        {
            var r2 = UnityEngine.Random.Range(0, 2);
            //Solo una Banana
            if (r2 == 0)
            {
                nObjeto = 3;
                numObjetoDisponible = 1;
                CanvasJugador.GetComponent<Animator>().SetBool("Platano", true);
                Debug.Log("Objeto actual: 1 Platano");
            }
            //3 Platanos
            else if (r2 == 1)
            {
                numObjetoDisponible = 3;
                nObjeto = 4;
                CanvasJugador.GetComponent<Animator>().SetBool("Platano", true);
                Debug.Log("Objeto actual: 3 Platanos");
            }
        }
        //Setas
        else if (r == 3)
        {
            var r2 = UnityEngine.Random.Range(0, 3);
            //Solo una Seta
            if (r2 == 0)
            {
                nObjeto = 5;
                numObjetoDisponible = 1;
                CanvasJugador.GetComponent<Animator>().SetBool("Seta", true);
                Debug.Log("Objeto actual: Seta");
            }
            //3 Setas
            else if (r2 == 1)
            {
                numObjetoDisponible = 3;
                nObjeto = 6;
                CanvasJugador.GetComponent<Animator>().SetBool("Seta", true);
                Debug.Log("Objeto actual: 3 Setas");
            }
            //Seta dorada
            else if (r2 == 2)
            {
                nObjeto = 7;
                numObjetoDisponible = 10;
                CanvasJugador.GetComponent<Animator>().SetBool("SetaD", true);
                Debug.Log("Objeto actual: Seta Dorada");
            }
        }
        //Estrella
        else if (r == 4)
        {
            nObjeto = 8;
            numObjetoDisponible = 1;
            CanvasJugador.GetComponent<Animator>().SetBool("Estrella", true);
            Debug.Log("Objeto actual: Star");
        }
        cajaOn = false;
        objetoSpawn = true;
        CanvasJugador.GetComponent<Animator>().SetBool("Transicion", false);
    }
    #endregion Methods

    #region Spawns
    //Spawnea la caja detras del jugador
    public void SpawnCajaMala()
    {
        GameObject caja = Instantiate(CajaMala);
        caja.transform.forward = player.transform.forward;
        caja.transform.right = player.transform.right;
        caja.transform.position = new Vector3((player.transform.position.x - player.transform.forward.x) - 1, player.transform.position.y + 0.1f, (player.transform.position.z - player.transform.forward.z) - 1);
        numObjetoDisponible--;
    }

    //Spawnea el platano detras del jugador
    public void SpawnPlatano()
    {
        GameObject platano = Instantiate(Platano);
        platano.transform.forward = player.transform.forward;
        platano.transform.right = player.transform.right;
        platano.transform.position = new Vector3((player.transform.position.x - player.transform.forward.x) - 1, player.transform.position.y, (player.transform.position.z - player.transform.forward.z) - 1);
        numObjetoDisponible--;
    }

    //Spawnea el platano detras del jugador (3 veces)
    public void SpawnPlatanox3()
    {
        GameObject platano = Instantiate(Platano);
        platano.transform.forward = player.transform.forward;
        platano.transform.right = player.transform.right;
        platano.transform.position = new Vector3((player.transform.position.x - player.transform.forward.x) - 1, player.transform.position.y, (player.transform.position.z - player.transform.forward.z) - 1);
        numObjetoDisponible--;
    }

    //Da velocidad al jugador en la direccion que esta mirando 
    public void SpawnSeta()
    {
        this.GetComponent<Rigidbody>().AddRelativeForce(this.transform.localPosition * 5000f * Time.deltaTime);
        numObjetoDisponible--;
    }

    //Da velocidad al jugador en la direccion que esta mirando (3 veces)
    public void SpawnSetax3()
    {
        this.GetComponent<Rigidbody>().AddRelativeForce(this.transform.localPosition * 5000f * Time.deltaTime);
        numObjetoDisponible--;
    }

    //Spawnea una concha hacia donde mire el jugador
    public void SpawnConcha()
    {
        GameObject concha = Instantiate(ConchaVerde);
        concha.transform.forward = player.transform.forward;
        concha.transform.position = new Vector3(player.transform.position.x + player.transform.forward.x, player.transform.position.y, player.transform.position.z + player.transform.forward.z);
        StartCoroutine(TiempoConchas(5f, concha));
        numObjetoDisponible--;
    }

    //Spawnea una concha hacia donde mire el jugador (3 veces)
    public void SpawnConchax3()
    {
        GameObject concha = Instantiate(ConchaVerde);
        concha.transform.forward = player.transform.forward;
        concha.transform.position = new Vector3(player.transform.position.x + player.transform.forward.x, player.transform.position.y, player.transform.position.z + player.transform.forward.z);
        StartCoroutine(TiempoConchas(5f, concha));
        numObjetoDisponible--;
    }

    //Da velocidad al jugador en la direccion que esta mirando (10 veces)
    public void SpawnSetaDorada()
    {
        this.GetComponent<Rigidbody>().AddRelativeForce(this.transform.localPosition * 4000f * Time.deltaTime);
        numObjetoDisponible--;
    }

    //Hace al jugador invulnerable a cualquier daño durante 8 segundos y le cambia el material, si empuja al otro jugador le hace daño
    public void SpawnEstrella()
    {
        var childColor = GetComponentInChildren<MeshRenderer>();
        childColor.material = StarMaterial;
        invencible = true;
        numObjetoDisponible--;
        StartCoroutine(Invencibilidad(8f));
    }
    #endregion Spawns

    #region Coll
    private void OnTriggerEnter(Collider collider)
    {
        //Colisiones con las cajas que dan los objetos
        if (collider.gameObject.CompareTag("Box"))
        {
            if (objetoObtenido == false)
            {
                cajaOn = true;
                CanvasJugador.GetComponent<Animator>().SetBool("Transicion", true);
                objetoObtenido = true;
                collider.gameObject.SetActive(false);
                StartCoroutine(TiempoCajas(tiempoRespawnCajas, collider.gameObject));
            }
        }

        //Colisiones con cajas/platanos/conchas, si el jugador no es invencible por el efecto de la estrella recibe daño y se elimina uno de sus globos
        if (collider.gameObject.CompareTag("ItemMalo"))
        {
            if (invencible == false)
            {
                Globos--;
                StartCoroutine(Stop());
                //Con esto le quito al jugador los globos que representan visualmente su vida, cauando se queda sin globos pierde
                var globoFenecido = GlobosJugador.Count() - 1;
                var globoQueFenece = GlobosJugador.ElementAt(globoFenecido);
                if (globoQueFenece == null)
                {
                    GlobosJugador.RemoveAt(globoFenecido);
                    globoFenecido = GlobosJugador.Count() - 1;
                    globoQueFenece = GlobosJugador.ElementAt(globoFenecido);
                }
                Destroy(globoQueFenece.gameObject);
                if (Globos <= 0)
                {
                    Morision = true;
                }
            }
            Destroy(collider.gameObject);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            jump = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Colision con el otro jugador cuando el tiene la estrella activa y este no
        if (collision.gameObject.name.Equals("Player2") && hitted == false && invencible == false && player2.GetComponent<PlayerController2>().invencible == true)
        {
            Globos--;
            Globos--;
            StartCoroutine(Stop());
            //Con esto le quito al jugador los globos que representan visualmente su vida, cauando se queda sin globos pierde
            var globoFenecido = GlobosJugador.Count() - 1;
            var globoQueFenece = GlobosJugador.ElementAt(globoFenecido);
            if (globoQueFenece == null)
            {
                GlobosJugador.RemoveAt(globoFenecido);
                globoFenecido = GlobosJugador.Count() - 1;
                globoQueFenece = GlobosJugador.ElementAt(globoFenecido);
            }
            Destroy(globoQueFenece.gameObject);
            if (Globos <= 0)
            {
                Morision = true;
            }
            hitted = true;
            StartCoroutine(WaitStar(2f));
        }
        //Colision con el otro jugador cuando el va muy rapido (ha usado la seta)
        if (collision.gameObject.name.Equals("Player2") && player2.GetComponent<Rigidbody>().velocity.magnitude > 23 && hitted == false)
        {
            Globos--;
            Globos--;
            StartCoroutine(Stop());
            //Con esto le quito al jugador los globos que representan visualmente su vida, cauando se queda sin globos pierde
            var globoFenecido = GlobosJugador.Count() - 1;
            var globoQueFenece = GlobosJugador.ElementAt(globoFenecido);
            if (globoQueFenece == null)
            {
                GlobosJugador.RemoveAt(globoFenecido);
                globoFenecido = GlobosJugador.Count() - 1;
                globoQueFenece = GlobosJugador.ElementAt(globoFenecido);
            }
            Destroy(globoQueFenece.gameObject);
            if (Globos <= 0)
            {
                Morision = true;
            }
            hitted = true;
            StartCoroutine(WaitStar(2f));
        }

    }
    #endregion Coll

    #region Coroutines
    private IEnumerator SetaDorada(float time)
    {
        yield return new WaitForSeconds(time);
        cajaOn = false;
    }

    private IEnumerator Invencibilidad(float time)
    {
        yield return new WaitForSeconds(time);
        //player.GetComponent<MeshRenderer>().material = BaseMaterial;
        var childColor = GetComponentInChildren<MeshRenderer>();
        childColor.material = BaseMaterial;
        invencible = false;
        Debug.Log("No Invensible");
    }

    private IEnumerator TiempoCajas(float time, GameObject caja)
    {
        yield return new WaitForSeconds(time);
        caja.SetActive(true);
    }

    private IEnumerator TiempoConchas(float time, GameObject concha)
    {
        yield return new WaitForSeconds(time);
        Destroy(concha);
    }

    private IEnumerator ObjetoRandomTimer(float time)
    {
        yield return new WaitForSeconds(time);
        ObjetoRandom();
        timer = false;
    }

    private IEnumerator Stop()
    {
        float tiempo = 0;
        while (tiempo < 1.5)
        {
            // Code to go left here
            tiempo += Time.deltaTime;
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            yield return null;
        }
    }

    private IEnumerator WaitStar(float time)
    {
        yield return new WaitForSeconds(time);
        hitted = false;
    }
    #endregion Coroutines
}